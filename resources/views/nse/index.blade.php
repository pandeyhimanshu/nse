<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>NSE India Stock List</title>
    <link rel="icon" href="{!! asset('images/title.png') !!}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('agripos/css/lib/base.css')}}">
    <link href="https://datatables.yajrabox.com/css/app.css" rel="stylesheet">
    <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">
    <script src="https://datatables.yajrabox.com/js/jquery.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/bootstrap.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
    <script src="https://datatables.yajrabox.com/js/handlebars.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.0.0/jquery.mark.min.js"></script>
    <link rel="stylesheet" href="https://datatables.yajrabox.com/highlight/styles/zenburn.css">
    <script src="https://pagead2.googlesyndication.com/pagead/js/r20210224/r20190131/reactive_library_fy2019.js"></script><script src="https://www.googletagservices.com/activeview/js/current/osd.js?cb=%2Fr20100101"></script><script src="https://partner.googleadservices.com/gampad/cookie.js?domain=datatables.yajrabox.com&amp;callback=_gfp_s_&amp;client=ca-pub-2399525660597307&amp;cookie=ID%3Deb7b615fa7919c10-226217212dc600b2%3AT%3D1614415158%3ART%3D1614415158%3AS%3DALNI_MbCY7urU0UWL5u-Xmy_MONKmtRD5w"></script><script src="https://pagead2.googlesyndication.com/pagead/js/r20210224/r20190131/show_ads_impl_fy2019.js" id="google_shimpl"></script><script src="https://datatables.yajrabox.com/highlight/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <style>
        #laravel_datatable th{
            font-size:13px;
        }
        #laravel_datatable td:nth-child(3){
            word-break: break-all;
        }
    </style>
</head>
<body>
<div id="loaders"></div>
<nav class="navbar navbar-light" style="background-color: #ddd;">
    <div class="btn-group pull-right" role="group" style="margin-top: 5px !important;">
        <button type="button" style="background: #1F6741;border-color: #1F6741;color: #ffffff;" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Auth::user()->name }}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</nav>
<br />
<div class="container box">
    <h3 align="center">NSE INDIA STOCK LIST</h3><br />
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="form-group col-md-4">
                    <label for="Symbol" class="control-label">Symbol</label>
                    <select id="Symbol" name="Symbol" class="form-control Symbol">
                        <option value="">--Select Company--</option>
                        @foreach($Symbol as $data)
                            <option value="{{$data->Symbol}}">{{$data->name}}</option>
                        @endforeach
                    </select>

                </div>
                <div class="col-md-2" style="margin-top: 20px !important;">
                    <button type="button" name="filter" id="btnFiterSubmitSearch" class="btn btn-info btn-sm">Filter</button>
                </div>
            </div><br>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="">
                    <table class="table table-bordered yajra-datatable" id="laravel_datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Symbol</th>
                            <th>Series</th>
                            <th>Date</th>
                            <th>Prev Close</th>
                            <th>Open Price</th>
                            <th>High Price</th>
                            <th>Low Price</th>
                            <th>Last Price</th>
                            <th>Close Price</th>
                            <th>Average Price</th>
                            <th>Total Traded Quantity</th>
                            <th>Turnover</th>
                            <th>No of Trades</th>
                            <th>Deliverable Qty</th>
                            <th>% Dly Qt to Traded Qty</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    $(window).on('load', function() {
        if ($('#loaders').length) {
            $('#loaders').delay(1000).fadeOut('slow', function() {
                $(this).remove();
            });
        }
    });

    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#laravel_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('usersList') }}",
                type: 'GET',
                data: function (d) {
                    console.log(d);
                    d.Symbol = $('#Symbol').val();
                }
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'Symbol', name: 'Symbol' },
                { data: 'Series', name: 'Series' },
                { data: 'Date', name: 'Date' },
                { data: 'Prev_Close', name: 'Prev_Close' },
                { data: 'Open_Price', name: 'Open_Price' },
                { data: 'High_Price', name: 'High_Price' },
                { data: 'Low_Price', name: 'Low_Price' },
                { data: 'Last_Price', name: 'Last_Price' },
                { data: 'Close_Price', name: 'Close_Price' },
                { data: 'Average_Price', name: 'Average_Price' },
                { data: 'Total_Traded_Quantity', name: 'Total_Traded_Quantity' },
                { data: 'Turnover', name: 'Turnover' },
                { data: 'No_of_Trades', name: 'No_of_Trades' },
                { data: 'Deliverable_Qty', name: 'Deliverable_Qty' },
                { data: 'Dly_Qt_to_Traded_Qty', name: 'Dly_Qt_to_Traded_Qty' },
            ]
        });
    });


    $('#btnFiterSubmitSearch').click(function(){
        $('#laravel_datatable').DataTable().draw(true);
    });
</script>
