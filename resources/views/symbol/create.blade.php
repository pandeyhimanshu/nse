@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="panel-heading clearfix mx-3 my-3">
                <div class="btn-group btn-group-sm pull-right" role="group">
                    <a href="{{ route('symbol.index') }}" title="Back">
                        <button class="btn btn-labeled btn-green mb-2" type="button">
                                           <span class="btn-label"><i class="fa fa-list"></i>
                                           </span>Back
                        </button>
                    </a>
                </div>
            </div>
            <div class="panel-body mx-3 my-3">
                <form autocomplete="off" method="POST" action="{{ route('symbol.store') }}" enctype="multipart/form-data" accept-charset="UTF-8"
                      id="create_customer_form" name="create_customer_form" class="form-horizontal">
                    {{ csrf_field() }}
                    @include ('symbol.form', ['Symbol' => null])
                    <div class="row ml-1">
                        <div class="col-md-offset-2 col-md-6">
                            <input class="btn btn-primary" type="submit" value="Add">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
