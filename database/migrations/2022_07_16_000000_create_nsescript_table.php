<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNsescriptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('nse_script', function (Blueprint $table) {
            $table->id();
            $table->string('Symbol')->nullable();
            $table->string('Series')->nullable();
            $table->date('Date')->nullable();
            $table->double('Prev_Close')->default(0);
            $table->double('Open_Price')->default(0);
            $table->double('High_Price')->default(0);
            $table->double('Low_Price')->default(0);
            $table->double('Last_Price')->default(0);
            $table->double('Close_Price')->default(0);
            $table->double('Average_Price')->default(0);
            $table->double('Total_Traded_Quantity')->default(0);
            $table->double('Turnover')->default(0);
            $table->double('No_of_Trades')->default(0);
            $table->double('Deliverable_Qty')->default(0);
            $table->double('Dly_Qt_to_Traded_Qty')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nse_script');
    }
}
