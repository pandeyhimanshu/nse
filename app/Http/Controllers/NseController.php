<?php
namespace App\Http\Controllers;
use App\Models\Symbol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Nse;
use Illuminate\Http\Response;
class NseController extends Controller
{
    public function index(){
        $Symbol = Symbol::where('status',(int)0)->get();
        return view('nse.index',compact('Symbol'));
    }

    public function nseList(){
        $usersQuery = Nse::query();
        $Symbol = (!empty($_GET["Symbol"])) ? ($_GET["Symbol"]) : ('');
        if($Symbol){
            $usersQuery->where("Symbol",$Symbol);
        }
        $shortURL = $usersQuery->select('*');
        return datatables()->of($shortURL)->make(true);
    }

    public function create(){
        return view('nse.create');
    }

    public function store(Request $request){
        $Nse = Nse::all();
        $file = $request->file('uploaded_file');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $fileSize = $file->getSize();
            $this->checkUploadedFileProperties($extension, $fileSize);
            $location = 'uploads';
            $file->move($location, $filename);
            $filepath = public_path($location . "/" . $filename);
            $file = fopen($filepath, "r");
            $importData_arr = array();
            $i = 0;
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                $num = count($filedata);
                if ($i == 0) {
                    $i++;
                    continue;
                }
                for ($c = 0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata[$c];
                }
                $i++;
            }
            fclose($file);
            $j = 0;
            foreach ($importData_arr as $importData) {
                $j++;
                try {
                    DB::beginTransaction();
                    Nse::create([
                        'Symbol' => $importData[0],
                        'Series' => $importData[1],
                        'Date' => $importData[2],
                        'Prev_Close' => $importData[3],
                        'Open_Price' => $importData[4],
                        'High_Price' => $importData[5],
                        'Low_Price' => $importData[6],
                        'Last_Price' => $importData[7],
                        'Close_Price' => $importData[8],
                        'Average_Price' => $importData[9],
                        'Total_Traded_Quantity' => $importData[10],
                        'Turnover' => $importData[11],
                        'No_of_Trades' => $importData[12],
                        'Deliverable_Qty' => $importData[13],
                        'Dly_Qt_to_Traded_Qty' => $importData[14]
                    ]);
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                }
            }
            $message = "$j records successfully uploaded";

        } else {
            throw new \Exception('No file was uploaded', Response::HTTP_BAD_REQUEST);
        }
        return redirect()->route('nse.create')->with('success_message', $message);
    }

    public function uploadContent(Request $request)
    {
        $file = $request->file('uploaded_file');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $this->checkUploadedFileProperties($extension, $fileSize);
            $location = 'uploads';
            $file->move($location, $filename);
            $filepath = public_path($location . "/" . $filename);
            $file = fopen($filepath, "r");
            $importData_arr = array();
            $i = 0;
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                $num = count($filedata);
                if ($i == 0) {
                    $i++;
                    continue;
                }
                for ($c = 0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata[$c];
                }
                $i++;
            }
            fclose($file);
            $j = 0;
            foreach ($importData_arr as $importData) {
                $j++;
                try {
                    DB::beginTransaction();
                    Nse::create([
                        'Symbol' => $importData[0],
                        'Series' => $importData[1],
                        'Date' => $importData[2],
                        'Prev_Close' => $importData[3],
                        'Open_Price' => $importData[4],
                        'High_Price' => $importData[5],
                        'Low_Price' => $importData[6],
                        'Last_Price' => $importData[7],
                        'Close_Price' => $importData[8],
                        'Average_Price' => $importData[9],
                        'Total_Traded_Quantity' => $importData[10],
                        'Turnover' => $importData[11],
                        'No_of_Trades' => $importData[12],
                        'Deliverable_Qty' => $importData[13],
                        'Dly_Qt_to_Traded_Qty' => $importData[14]
                    ]);
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                }
            }
            return response()->json([
                'message' => "$j records successfully uploaded"
            ]);
        } else {
            throw new \Exception('No file was uploaded', Response::HTTP_BAD_REQUEST);
        }
    }
    public function checkUploadedFileProperties($extension, $fileSize)
    {
        $valid_extension = array("csv", "xlsx");
        $maxFileSize = 2097152; // Uploaded file size limit is 2mb
        if (in_array(strtolower($extension), $valid_extension)) {
            if ($fileSize <= $maxFileSize) {
            } else {
                throw new \Exception('No file was uploaded', Response::HTTP_REQUEST_ENTITY_TOO_LARGE); //413 error
            }
        } else {
            throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
        }
    }
}
