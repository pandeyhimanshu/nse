<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Nse extends Model
{
    use HasFactory;
    protected $table = 'nse_script';
    protected $fillable = [
        'Symbol',
        'Series',
        'Date',
        'Prev_Close',
        'Open_Price',
        'High_Price',
        'Low_Price',
        'Last_Price',
        'Close_Price',
        'Average_Price',
        'Total_Traded_Quantity',
        'Turnover',
        'No_of_Trades',
        'Deliverable_Qty',
        'Dly_Qt_to_Traded_Qty'
    ];
}
