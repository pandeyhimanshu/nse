<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\NseController;
use App\Http\Controllers\SymbolController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::group(['prefix' => 'nse'], function () {
    Route::get('/', [NseController::class, 'index'])->name('nse.index');
    Route::get('/short_urls_list', [NseController::class, 'nseList'])->name('usersList');
    Route::get('/create', [NseController::class, 'create'])->name('nse.create');
    Route::post('/', [NseController::class, 'store'])->name('nse.store');
});


Route::group(['prefix' => 'symbol'], function () {
    Route::get('/', [SymbolController::class, 'index'])->name('symbol.index');
    Route::get('/symbol_list', [SymbolController::class, 'symbolList'])->name('symbolList');
    Route::get('/create', [SymbolController::class, 'create'])->name('symbol.create');
    Route::post('/', [SymbolController::class, 'store'])->name('symbol.store');
});


